// Importar las funciones necesarias de los SDKs que se necesitan
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Agregar SDKs para los productos de Firebase que se desean usar
// https://firebase.google.com/docs/web/setup#available-libraries

import { getDatabase, ref as refS, onValue } 
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

// Configuración de Firebase
const firebaseConfig = {
  apiKey: "AIzaSyC2THGtWUos9hUT_XEo0vjgQ7aWLNb_nbs",
  authDomain: "adminpastel-1f4b9.firebaseapp.com",
  databaseURL: "https://adminpastel-1f4b9-default-rtdb.firebaseio.com",
  projectId: "adminpastel-1f4b9",
  storageBucket: "adminpastel-1f4b9.appspot.com",
  messagingSenderId: "550473494532",
  appId: "1:550473494532:web:d1357be374ef09d9214551"
};

// Inicializar Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

// Declarar variables globales
var codigo = "";
var tematica = "";
var objetos = "";
var descripcion = "";
var urlImag = "";

// Listar productos
listarProductos(tematica);

function listarProductos() {
    const titulo = document.getElementById("titulo");
    const section = document.getElementById('secArticulos');
    titulo.innerHTML = tematica;

    const dbRef = refS(db, 'Pasteles');

    // Limpiar la sección antes de llenarla
    section.innerHTML = "";

    onValue(dbRef, (snapshot) => {
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const data = childSnapshot.val();

            section.innerHTML += "<div class='card'>" +
                "<img id='urlImag' src='" + data.urlImag + "' alt='' width='200'>" +
                "<h1 id='tematica' class='title'>" + data.tematica + ": " + data.objetos + "</h1>" +
                "<p id='descripcion' class='anta-regular'>" + data.descripcion + "</p>" +
                "<button>Más Información</button>" +
                "</div>";
        });
    }, { onlyOnce: true });
}

